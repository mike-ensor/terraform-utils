#!/bin/bash

if [ -z "$REMOTE_BUCKET" ]  && [ -z "$2" ]; then echo "Pass a remote GCS Bucket if not defining with \$REMOTE_BUCKET";  exit 1; fi

# TODO Add something that filters out bucket and adds filtering
array=($(gsutil ls gs://$REMOTE_BUCKET))

for (( i=0; i<${#array[@]}; i++ ))
do
    item=${array[$i]}
    item=${item//"gs://${REMOTE_BUCKET}/"/""} # Removes the bucket prefix
    item=${item//"/"/""} # removes last "/"
    echo "$((i+1)): ${item}"
done
