#!/bin/bash

if [ -z "$1" ]; then echo "Please set a State File Prefix";  exit 1; fi

if [ -z "$REMOTE_BUCKET" ]  && [ -z "$2" ]; then echo "Pass a remote GCS Bucket if not defining with \$REMOTE_BUCKET";  exit 1; fi

if [ -z "$CRED_FILE_LOC" ]; then
    export CRED_FILE_LOC=$GOOGLE_APPLICATION_CREDENTIALS
fi

export CURR_DIR=$(pwd)
export PREFIX=$1

# TODO: Build this into some real feature
if [ ! -z "$TF_VARS_module_oauth_token+x" ]; then 
    echo "Using TF State Prefix: ${PREFIX}"
    sed -i -e "s/__TOKEN__/${TF_VARS_module_oauth_token}/g" ${CURR_DIR}/main.tf
fi

echo "###########################################"
echo ""
echo "Using Bucket: ${REMOTE_BUCKET}"
echo "Prefix: ${PREFIX}"
echo ""
echo "###########################################"
echo ""

terraform init -backend-config="credentials=${CRED_FILE_LOC}" -backend-config="bucket=${REMOTE_BUCKET}" -backend-config="prefix=${PREFIX}"

if [ -z "$TF_VARS_module_oauth_token" ]; then 
    echo " "
    echo "DON'T FORGET TO 'git checkout -- main.tf'"
    echo " "
fi