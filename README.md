# Terraform State Manager (Google)

# Requirements
* `gsutil`
* `gcloud` -- configured in using Service Account with visibility to state bucket
* Environment variables
  * State Bucket - 
  * Google Service Account JSON = $GOOGLE_APPLICATION_CREDENTIALS
    * If default gcloud credentials are not desired, override with $CRED_FILE_LOC

# Operations
* Setup local directory with remote state
* Remove remote state (project-level)
